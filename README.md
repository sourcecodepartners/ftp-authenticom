# FtpAuthenticom

This project rocks and uses MIT-LICENSE.

---
###Geting started:
`bin/rails generate ftp_authenticom:install`

>will create models and migrations for you


dont forget to run new migrations

`bin/rake db:migrate`

### Usage:

####FtpAuthenticom::Ftp
* instance_method  #download_files(ftp_configuration) :

>download files from listed in config ftp server and move them in /tmp, method returns array of filepaths where downloaded files located

#####Example:
```ruby
config = Hashie::Mash.new
config.host = 'localhost' #defaults
config.port = '21' #defaults
config.username = 'foo'
config.password = 'bar'
config.top_folder_name = '/' #defaults
FtpAuthenticate::Ftp.new.download_files(config) #=> ['../tmp/*', ...]
```
---
####FtpAuthenticom::Csv
* class_method  #insert_file(file_path) :

>parse structured csv file by path passed as argument and insert this data to SlFile and SlFileRow tables

#####Example:
```ruby
csv_file_path #=> '../../foo.csv'
FtpAuthenticom::Csv.insert_file(csv_file) #=> true if success
```
---
####FtpAuthenticom::Client
* instance_method  #process(ftp_configuration = nil) :

>full handle of ftp download and parse

#####Example:
```ruby
client = FtpAuthenticom::Client.new
client.process # all stuff goes implicitly
```






