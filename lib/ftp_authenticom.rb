require 'ftp_authenticom/ftp'
require 'ftp_authenticom/csv'
require 'ftp_authenticom/client'
require 'ftp_authenticom/version'

class FtpAuthenticom::Configuration
  include ActiveSupport::Configurable

  config_accessor :host
  config_accessor :port
  config_accessor :username
  config_accessor :password
  config_accessor :account
  config_accessor :top_folder_name
end

module FtpAuthenticom
  def self.configuration
    @configuration ||= FtpAuthenticom::Configuration.new
  end

  def self.configure
    yield configuration
  end

  configure do |config|
    config.host = 'localhost'
    config.port = 21
    config.username = 'anonymous'
    config.password = nil
    config.account = nil
    config.top_folder_name = '/'
  end

end

