require 'smarter_csv'

module FtpAuthenticom
  module Csv
    class << self
      def insert_file(file_path, dealership_id=nil)
        file = SlFile.new(:name => File.basename(file_path))
        file.dealership_id = dealership_id if dealership_id.present?
        options = {:chunk_size => 20, :col_sep => "\t", :key_mapping => {:customer_birth_date => :customer_birth_on}, :header_converter => lambda {|x| x.underscore}}
        rows = []
        file_columns = []
        n = SmarterCSV.process(file_path, options) do |chunk|
          # we're passing a block in, to process each resulting hash / row (block takes array of hashes)
          # when chunking is enabled, there are up to :chunk_size hashes in each chunk
          chunk.each do |row|
            logger.info row.inspect
            file_columns |= row.keys
            rows << row
          end
        end

        file.columns = file_columns
        file.save!

        bulk_insert(rows.map { |r| r.merge({ created_at: Time.now, updated_at: Time.now, sl_file_id: file.id}) })

        logger.info "File #{file.name}: processed #{n} rows."
      end

      private

      def bulk_insert(rows)
        return unless rows.any?

        all_keys = rows.map(&:keys).reduce(:|).select { |k| csv_column_present?(k) }

        file_columns = all_keys.map { |k| ActiveRecord::Base.connection.quote_column_name(k) }
        columns_str = file_columns.join(', ')
        inserts = rows.map { |row|
          values = all_keys.map { |k| ActiveRecord::Base.connection.quote(parse_csv_column k, row[k]) }
          values_str = values.join(', ')
          "(#{values_str})"
        }

        sql = "INSERT INTO sl_file_rows (#{columns_str}) VALUES #{inserts.join(', ')}"
        logger.info sql

        ActiveRecord::Base.connection.execute(sql)
      end

      def csv_column_present?(column)
        not SlFileRow.columns_hash[column.to_s].nil?
      end

      def parse_csv_column(column, value)
        if %[deal_book_date accounting_date entry_date].include?(column.to_s)
          parse_formated_date(value)
        else
          SlFileRow.columns_hash[column.to_s].type_cast_from_user(value)
        end
      end

      def parse_formated_date(value, format = "%m/%d/%Y")
        format ||= "%m/%d/%Y"
        begin
          Date.strptime(value, format)
        rescue
          nil
        end
      end

      def logger
        Rails.logger
      end

    end
  end
end