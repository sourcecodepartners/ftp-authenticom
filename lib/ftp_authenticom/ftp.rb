require 'fileutils'
require 'net/ftp'

module FtpAuthenticom
  class Ftp
    def download_files(ftp_settings)
      connect(ftp_settings)
      file_list = get_file_list(ftp_settings.top_folder_name)
      downloaded_files = download_file_list(file_list)
      disconnect()
      downloaded_files
    end

    private

    def download_file_list(file_list)
      downloaded_files = []
      file_list.each do |file|
        local_path = "#{temp_path}#{File.basename(file)}"
        @ftp.getbinaryfile(file, local_path) unless File.exist? local_path
        downloaded_files << local_path
      end
      downloaded_files
    end

    def get_file_list(folder_name)
      FileUtils::mkdir_p temp_path unless File.exist? temp_path
      FileUtils::rm_rf "#{temp_path}/*"
      @ftp.chdir(folder_name)
      @ftp.nlst('*TM*SL*')
    end

    def temp_path
      "#{Rails.root}/tmp/ftp/"
    end

    def connect(ftp_settings)
      @ftp = Net::FTP.new
      @ftp.debug_mode = true
      @ftp.passive = true
      @ftp.connect(ftp_settings.host, ftp_settings.port)
      @ftp.login(ftp_settings.userename, ftp_settings.password)
    end

    def disconnect
      @ftp.close
    end
  end
end
