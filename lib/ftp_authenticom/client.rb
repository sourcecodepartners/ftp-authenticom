module FtpAuthenticom
  class Client
    attr_accessor :configuration

    def process(dealership_id = nil)
      files_to_process = FtpAuthenticom::Ftp.new.download_files(configuration)
      files_to_process.each do |file|
        filename = File.basename(file)
        logger.info "Process file: #{file}"
        begin
          FtpAuthenticom::Csv.insert_file(file, dealership_id) unless SlFile.find_by(:name => filename)
        rescue Exception => exc
          logger.error "cannot process file #{filename}!!!!! {#{exc.message}}"
          logger.error $!.backtrace
          sl_file = SlFile.find_by(name: filename)
          sl_file.destroy if sl_file.present?
        end
      end

    end

    def initialize(config = nil)
      if config.present?
        set_config(config) 
      else
        set_config(FtpAuthenticom.configuration)
      end
    end

    private

    def set_config(config)
      @configuration = FtpAuthenticom::Configuration.new
      @configuration.host =            config.host
      @configuration.port =            config.port
      @configuration.username =        config.username
      @configuration.password =        config.password
      @configuration.account =         config.account
      @configuration.top_folder_name = config.top_folder_name
    end
  end
end