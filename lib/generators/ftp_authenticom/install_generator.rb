#lib/generators/ftp_authenticom/install_generator.rb

require 'rails/generators'
module FtpAuthenticom
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path(File.join(File.dirname(__FILE__), 'templates'))
    desc "Installs ftp_authenticom gem to your application"

    def create_initializer_file
      template 'initializer.rb', 'config/initializers/ftp_authenticom.rb'
    end

    def create_sl_file_model
      template 'sl_file.rb', 'app/models/sl_file.rb'
    end

    def create_sl_file_row_model
      template 'sl_file_row.rb', 'app/models/sl_file_row.rb'
    end

    def create_migration
      template 'migration.rb', "db/migrate/#{Time.now.utc.strftime("%Y%m%d%H%M%S")}_create_ftp_authenticom.rb"
    end

  end
end