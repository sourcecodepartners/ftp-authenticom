$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ftp_authenticom/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ftp_authenticom"
  s.version     = FtpAuthenticom::VERSION
  s.authors     = ["SourceCodePartners"]
  s.email       = ["contact@sourcecodepartners.com"]
  s.homepage    = "https://bitbucket.org/sourcecodepartners/ftp-authenticom"
  s.summary     = 'Gem for handling downloads from authenticom remote ftp and ' +
                  'automaticaly parse strong structured files to ActiveRecord models'
  s.description = "FtpAuthenticom::Ftp and FtpAuthenticom::Csv modules; FtpAuthenticom::Client for full processing + generation of sales models"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.0"

  s.add_development_dependency "pg"
  s.add_development_dependency "mocha"
end
