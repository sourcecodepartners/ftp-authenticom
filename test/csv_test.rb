require 'test_helper'

class CsvTest < ActiveSupport::TestCase

  test 'truth' do
    assert_kind_of Module, FtpAuthenticom::Csv 
  end

  test "#insert_file" do
    path = File.expand_path('../../test/files/TM1001_20141107_0410_SL.TXT', __FILE__)
    assert_difference('SlFileRow.count', 10) do
      FtpAuthenticom::Csv.insert_file(path)
    end
  end

  test "#parse_int_csv_column" do
    assert_equal 45, FtpAuthenticom::Csv.send(:parse_csv_column, :day_to_first_payment, "45,000")
  end

  test "#parse_decimal_csv_column" do
    assert_equal 45.25, FtpAuthenticom::Csv.send(:parse_csv_column, :cash_price, "45.25")
  end
end
