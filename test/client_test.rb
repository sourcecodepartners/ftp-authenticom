require 'test_helper'

class ClientTest < ActiveSupport::TestCase

  test "instance has copy of configuration" do
    client = FtpAuthenticom::Client.new
    assert_equal client.configuration.host, FtpAuthenticom.configuration.host
    assert_equal client.configuration.port, FtpAuthenticom.configuration.port
    assert_equal client.configuration.username, FtpAuthenticom.configuration.username
    assert_equal client.configuration.password, FtpAuthenticom.configuration.password
  end

  test "can manage instance-config" do
    client = FtpAuthenticom::Client.new
    client.configuration.host = 'google.com'
    assert_equal client.configuration.host, 'google.com'
    assert_equal FtpAuthenticom.configuration.host, 'localhost'
  end
  
end
