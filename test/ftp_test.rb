require 'test_helper'

class FtpTest < ActiveSupport::TestCase
  test "truth" do
    assert_kind_of Class, FtpAuthenticom::Ftp
  end

  # TODO: spy Net::FTP and assert messages
   
  # poor message expectation
  test '##download_files' do
    subject.stubs(:connect)
    subject.stubs(:disconnect)
    subject.expects(:get_file_list)
    subject.expects(:download_file_list)
    subject.download_files(config)
  end

  private

  def subject
    @ftp ||= FtpAuthenticom::Ftp.new
  end 

  def config
    FtpAuthenticom.configuration
  end

end
