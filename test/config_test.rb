require 'test_helper'

class ConfigTest < ActiveSupport::TestCase
  test "has configuration" do
    assert_kind_of FtpAuthenticom::Configuration, FtpAuthenticom.configuration
  end

  test "has default values" do
    assert_equal FtpAuthenticom.configuration.host, 'localhost'
  end
end
