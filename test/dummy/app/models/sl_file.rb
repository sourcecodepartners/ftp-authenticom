class SlFile < ActiveRecord::Base

  belongs_to :dealership
  has_many :sl_file_rows, dependent: :destroy, inverse_of: :sl_file

  accepts_nested_attributes_for :sl_file_rows, :allow_destroy => true

  serialize :columns, Array

end