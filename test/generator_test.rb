require 'test_helper'

class InstallGeneratorTest < Rails::Generators::TestCase
  tests FtpAuthenticom::InstallGenerator
  destination File.expand_path("../tmp", File.dirname(__FILE__))

  setup :prepare_destination

  test "assert initializer properly created" do
    run_generator
    assert_file "config/initializers/ftp_authenticom.rb", /FtpAuthenticom.configure/
  end

  test "models created" do
    run_generator
    assert_file "app/models/sl_file.rb"
    assert_file "app/models/sl_file_row.rb"
  end

  test "migration created" do
    run_generator
    assert_migration "db/migrate/create_ftp_authenticom.rb"
  end


end